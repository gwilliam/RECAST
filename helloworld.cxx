#include<iostream>
#include<fstream>

int main(int argc, char** argv) {
  std::ofstream file;
  std::cout << "Hello World.  We will write a message to " << argv [1] << std::endl;
  file.open(argv[1]);
  file << "Writing this to a file.  My message is " << argv[2] << std::endl;
  return 0;
}
